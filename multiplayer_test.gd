extends Node2D

var peer = ENetMultiplayerPeer.new()
@export var player_scene : PackedScene
var server : bool
var ip : String
var port : int

func _ready():
	if server:
		peer.create_server(port)
		multiplayer.multiplayer_peer = peer
		multiplayer.peer_connected.connect(_add_player)
		_add_player()
	else:
		peer.create_client(ip, port)
		multiplayer.multiplayer_peer = peer

func _add_player(id = 1):
	var player = player_scene.instantiate()
	player.name = str(id)
	call_deferred("add_child", player)

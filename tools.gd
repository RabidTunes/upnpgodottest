extends Node

const ERROR_STRING : String = ""
const ERROR_INT : int = -1
const _IP_PATTERN := "^((25[0-5]|(2[0-4]|1\\d|[1-9]|)\\d).?\\b){4}$"
@onready var _IP_REGEX : RegEx = RegEx.new()
const _BASE32_CHARACTERS = "0123456789ABCDEFGHXJKLMNZPQRSTUV"
const MIN_VALID_PORT : int = 1025


func _ready():
	_IP_REGEX.compile(_IP_PATTERN)


func get_upnp_code(ip : String, port : int) -> String:
	var hex_code =  port_to_hex(port - MIN_VALID_PORT) + ip_to_hex(ip)
	var base_32 = String().num_int64(number_from_hex(hex_code), 32, true)
	return base_32.replace("I", "X").replace("O", "Z") # Remove confusing characters O/0 I/1


func get_ip_from_upnp_code(code : String) -> String:
	if not _is_valid_upnp_code(code):
		return ERROR_STRING
	var iphex = _convert_upnp_code_into_hex(code).substr(4, 8)
	return ip_from_hex(iphex)


func get_port_from_upnp_code(code : String) -> int:
	if not _is_valid_upnp_code(code):
		return ERROR_INT
	var porthex = _convert_upnp_code_into_hex(code).substr(0, 4)
	return port_from_hex(porthex) + MIN_VALID_PORT


func _convert_upnp_code_into_hex(code : String) -> String:
	return number_to_hex(_convert_base32(code), 12)


func ip_to_hex(ip : String) -> String:
	if not _is_valid_ip(ip):
		return ERROR_STRING
	var split : PackedStringArray = ip.split(".")
	return (number_to_hex(split[0].to_int()).lpad(2, "0") 
			+ number_to_hex(split[1].to_int()).lpad(2, "0")
			+ number_to_hex(split[2].to_int()).lpad(2, "0")
			+ number_to_hex(split[3].to_int()).lpad(2, "0"))


func port_to_hex(port : int) -> String:
	if not _is_valid_port(port):
		return ERROR_STRING
	return number_to_hex(port).lpad(4, "0")


func number_to_hex(number : int, padding : int = 0) -> String:
	return String().num_int64(number, 16, true).lpad(padding, "0")


func ip_from_hex(hex : String) -> String:
	if not _is_valid_ip_hex(hex):
		return ERROR_STRING
	var result : String = (str(number_from_hex(hex.substr(0,2))) + "."
							+ str(number_from_hex(hex.substr(2,2))) + "."
							+ str(number_from_hex(hex.substr(4,2))) + "."
							+ str(number_from_hex(hex.substr(6,2))))
	if not _is_valid_ip(result):
		return ERROR_STRING
	return result


func port_from_hex(hex: String) -> int:
	if not _is_valid_port_hex(hex):
		return ERROR_INT
	var port : int = number_from_hex(hex)
	if not _is_valid_port(port):
		return ERROR_INT
	return port


func number_from_hex(hex : String) -> int:
	return hex.hex_to_int()


func _is_valid_upnp_code(code : String) -> bool:
	if code.length() <= 0:
		return false
	for char in code:
		if char not in _BASE32_CHARACTERS:
			return false
	return true


func _is_valid_ip(ip: String) -> bool:
	if _IP_REGEX.search(ip): # Only ipv4 supported
		return true
	return false


func _is_valid_port(port: int) -> bool:
	return port >= 0 and port <= 65535


func _is_valid_ip_hex(ip_hex : String) -> bool:
	return ip_hex.length() == 8


func _is_valid_port_hex(port_hex : String) -> bool:
	return port_hex.length() == 4


func _convert_base32(base32_string: String) -> int:
	# Convert the base32 string to uppercase for consistency.
	var uppercase_string = base32_string.to_upper()

	var result = 0
	var multiplier = 1

	# Loop through the base32 string from right to left.
	for i in range(uppercase_string.length() - 1, -1, -1):
		var char = uppercase_string[i]
		var value = _BASE32_CHARACTERS.find(char)

		# If the character is not found in the base32_chars string, return an error.
		if value == -1:
			return ERROR_INT

		# Multiply the value by the current multiplier and add it to the result.
		result += value * multiplier

		# Update the multiplier for the next iteration.
		multiplier *= 32

	return result

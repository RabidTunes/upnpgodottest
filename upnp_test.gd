extends Node

@onready var net_menu = $NetMenu
@onready var label_container := $LabelContainer
@onready var code_label := $LabelContainer/Code
var upnp : UPNP
@export var multiplayer_stage : PackedScene
const MIN_PORT : int = 1025
const MAX_PORT : int = 65535
var _port : int

func _ready():
	upnp = UPNP.new()
	var discover_result = upnp.discover()
	
	var local_addresses : PackedStringArray = IP.get_local_addresses()
	var filtered : Array[String] = []
	for address in local_addresses:
		if address.begins_with("192.168") or address.begins_with("10.") or address.begins_with("172.16"):
			filtered.append(address)
	
	if discover_result == UPNP.UPNP_RESULT_SUCCESS:
		print("Discover success")
		if upnp.get_gateway() and upnp.get_gateway().is_valid_gateway():
			print("Gateway valid")
			for port in range(MIN_PORT, MAX_PORT):
				for address in filtered:
					upnp.get_gateway().set_igd_our_addr(address)
					var map_result_udp = upnp.add_port_mapping(port, port, "godot_udp", "UDP", 0)
					var map_result_tcp = upnp.add_port_mapping(port, port, "godot_tcp", "TCP", 0)
				
					if not map_result_udp == UPNP.UPNP_RESULT_SUCCESS:
						map_result_udp = upnp.add_port_mapping(port, port, "", "UDP")
					
					if not map_result_tcp == UPNP.UPNP_RESULT_SUCCESS:
						map_result_tcp = upnp.add_port_mapping(port, port, "", "TCP")
		
					if map_result_udp == UPNP.UPNP_RESULT_SUCCESS and map_result_tcp == UPNP.UPNP_RESULT_SUCCESS:
						_port = port
						print("Mapped port:")
						print(_port)
						code_label.set_text(Tools.get_upnp_code(upnp.query_external_address(), _port))
						net_menu.enable_hosting()
						return
					
			print("No ports could be used")
			net_menu.disable_hosting()
		else:
			print("Gateway not valid")
			net_menu.disable_hosting()
	else:
		print("Discover failure")
		net_menu.disable_hosting()


func _print_upnp_devices():
	for i in range(upnp.get_device_count()):
		var device = upnp.get_device(i)
		print(device.description_url)
		print(device.igd_control_url)
		print(device.igd_our_addr)
		print(device.igd_service_type)
		print(device.igd_status)
		print(device.service_type)


func _remove_mappings():
	upnp.delete_port_mapping(_port, "UDP")
	upnp.delete_port_mapping(_port, "TCP")


func _on_net_menu_host():
	net_menu.queue_free()
	label_container.visible = true
	var stage = multiplayer_stage.instantiate()
	stage.server = true
	stage.port = _port
	call_deferred("add_child", stage)


func _on_net_menu_join(ip, port):
	net_menu.queue_free()
	var stage = multiplayer_stage.instantiate()
	stage.server = false
	stage.ip = ip
	stage.port = _port
	call_deferred("add_child", stage)

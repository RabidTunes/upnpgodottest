extends Control

signal host
signal join(ip : String, port : int)

@onready var host_info : Label = $Container/HostInfo
@onready var host_button : Button = $Container/HostButton
@onready var join_input : LineEdit = $Container/JoinInput
@onready var join_button : Button = $Container/JoinButton
@onready var join_info : Label = $Container/JoinInfo
@onready var join_timer : Timer = $JoinErrorMessageTimer

func enable_hosting():
	host_info.modulate = Color.MEDIUM_AQUAMARINE
	host_info.set_text("You can host!")
	host_button.set_disabled(false)


func disable_hosting():
	host_info.modulate = Color.ORANGE_RED
	host_info.set_text("You router settings won't allow hosting")
	host_button.set_disabled(true)


func _on_host_button_pressed():
	host_button.set_disabled(true) # Prevent double click
	host_info.modulate = Color.WHITE
	host_info.set_text("Loading...") # Just in case
	emit_signal("host")


func _on_join_button_pressed():
	var code : String = join_input.get_text().to_upper()
	join_input.set_text(code) # Uppercase it so player understands it has to be upper

	var ip : String = Tools.get_ip_from_upnp_code(code)
	var port : int = Tools.get_port_from_upnp_code(code)
	if ip == Tools.ERROR_STRING or port == Tools.ERROR_INT:
		join_info.set_text("Code is not valid, try again")
		join_timer.start()
		return
	join_button.set_disabled(true)
	emit_signal("join", ip, port)


func _on_join_error_message_timer_timeout():
	join_info.set_text("")
